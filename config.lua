-- Until I create some sort of GUI to configure these, you have to make do
-- with setting up your phrases in this file.
--
-- vim:ts=4:sw=4:sts=4:et:
--
--
-- Skill name must match exactly.
-- speakChance: (optional) override the main speakChance setting.
-- isUrgent: skill will always be spoken. Use only for spells cast rarely.
-- phrases: a set of phrases to randomly choose from.
--    channel: CMD_SAY, CMD_PARTY, CMD_TELL, CMD_EMOTE, CMD_SHOUT
--    text:    the text to be spoken
--    sayself: if false, then whenever you have yourself as your friendly
--             target, or no friendly target, this phrase will not be spoken.
--             (Except for heals, you'll probably want to leave this out)
--
-- Don't forget the commas.
-- Remember that skill names and phrase text strings must be prefixed with an L
-- String replacements:
--   %T - hostile target
--   %F - friendly target
--
-- [L"<skill  name>"] = {
--     speakChance = <chance>,
--     isUrgent = <true|false>,
--     phrases = {
--         {channel=<channel>, text=L"<text1>", sayself=<true|false>},
--         {channel=<channel>, text=L"<text2>", sayself=<true|false>},
--         {channel=<channel>, text=L"<text3>", sayself=<true|false>},
--     }
-- },
--
-- example:
--
-- [1] = {
--     name = L"'Ey, Quit Bleedin'",
--     isUrgent = false,
--     phrases = {
--         {channel=CMD_SAY, text=L"Quit bleedin', %F!", sayself=false},
--     }
-- },
--
--

TOL_CONFIGS = {}

local CMD_SAY = ThinkOutLoud.CMD_SAY
local CMD_TELL = ThinkOutLoud.CMD_TELL
local CMD_PARTY = ThinkOutLoud.CMD_PARTY
local CMD_EMOTE = ThinkOutLoud.CMD_EMOTE
local CMD_SHOUT = ThinkOutLoud.CMD_SHOUT


--------------------------------------------------------------------------
--  Edit below this line.
--------------------------------------------------------------------------

TOL_CONFIGS["DEFAULT"] = {}
TOL_CONFIGS["DEFAULT"].speakChance = 1.00
TOL_CONFIGS["DEFAULT"].skills = {

-- 50% chance of saying Aiiyeeeee! when fleeing.
[1] = {
    name = L"Flee",
    speakChance = 0.50,
    isUrgent = false,
    phrases = {
        {channel=CMD_SAY, text=L"Aiiyeeeee!"},
    }
},
-- AM's ressurection
[2] = {
    name = L"Gift of Life",
    speakChance = 1.0,
    isUrgent = true,
    phrases = {
        {channel=CMD_SAY, text=L"Isha, allow to %F breath again!", sayself=false},
    }
},
-- WP's ressurection
[3] = {
    name = L"Breath of Sigmar",
    speakChance = 1.0,
    isUrgent = true,
    phrases = {
        {channel=CMD_SAY, text=L"For the Sigmar's will, %F stand and fight!", sayself=false},
    }
},
-- RP's ressurection
[4] = {
    name = L"Rune of Life",
    speakChance = 1.0,
    isUrgent = true,
    phrases = {
        {channel=CMD_SAY, text=L"Live %F - there is too many grudges left!", sayself=false},
    }
},
-- DOK's ressurection
[5] = {
    name = L"Stand, Coward!",
    speakChance = 1.0,
    isUrgent = true,
    phrases = {
        {channel=CMD_SAY, text=L"Oh %F how dare you was to die? Stand up!", sayself=false},
    }
},
-- Shaman's ressurection
[6] = {
    name = L"Gedup!",
    speakChance = 1.0,
    isUrgent = true,
    phrases = {
        {channel=CMD_SAY, text=L"Hey %F gedup and smash sumfin!", sayself=false},
    }
},
-- Zealot's ressurection
[7] = {
    name = L"Tzeentch Shall Remake You",
    speakChance = 1.0,
    isUrgent = true,
    phrases = {
        {channel=CMD_SAY, text=L"Reanimating %F - Tzeentch will play you again!", sayself=false},
    }
},
-- Tank's guard
[8] = {
    name = L"Guard",
    speakChance = 1.0,
    isUrgent = true,
    phrases = {
        {channel=CMD_PARTY, text=L"Guarding %F now", sayself=false},
    }
},
-- BO's guard
[9] = {
    name = L"Save Da Runts",
    speakChance = 1.0,
    isUrgent = true,
    phrases = {
        {channel=CMD_PARTY, text=L"Savin %F now!", sayself=false},
    }
},
-- 4th tanks morale
[10] = {
    name = L"Immaculate Defense",
    speakChance = 1.0,
    isUrgent = true,
    phrases = {
        {channel=CMD_PARTY, text=L"-75% incoming damage for the next 10 sec!"},
    }
},
-- 3rd healers morale
[11] = {
    name = L"Divine Protection",
    speakChance = 1.0,
    isUrgent = true,
    phrases = {
        {channel=CMD_PARTY, text=L"Up to 4500 damage will be absorbed in the next 10 sec!"},
    }
},
-- 3rd tanks morale
[12] = {
    name = L"Distracting Bellow",
    speakChance = 1.0,
    isUrgent = false,
    phrases = {
        {channel=CMD_SHOUT, text=L" %T and all enemies in 30ft will deal only half damage in in the next 10 sec!"},
    }
},

} -- end of TOL_CONFIGS["DEFAULT"].skills, don't delete this bracket. :P

--------------------------------------------------------------------------
-- Stop Editing here
--------------------------------------------------------------------------