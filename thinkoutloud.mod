<?xml version="1.0" encoding="UTF-8"?>
<!-- vim:ts=3:sw=3:sts=3:et:syntax=xml:
-->
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">   
  <UiMod name="ThinkOutLoud" version="1.1.6" date="10/04/2008" >      
    <Author name="thanners" email="thanners@ninjasaurus.net" />      
    <Description text="Offers various options for announcing when you use abilities. Few templates added by SilverWF" />      
    <VersionSettings gameVersion="1.9.9" />      
    <Dependencies>         
      <Dependency name="EA_ChatWindow" />         
      <Dependency name="LibSlash" />      
    </Dependencies>             
    <Files>         
      <File name="thinkoutloud.lua" />         
      <File name="tolset.lua" />         
      <File name="tolskill.lua" />         
      <File name="config.lua" />         
      <File name="tolsettingsui.xml" />         
      <File name="tolsettingsui.lua" />      
    </Files>      
    <SavedVariables>         
      <SavedVariable name="ThinkOutLoud.PersistentSettings" />      
    </SavedVariables>             
    <OnInitialize>         
      <CallFunction name="ThinkOutLoud.Initialize" />      
    </OnInitialize>      
    <OnUpdate>         
      <CallFunction name="ThinkOutLoud.Update" />      
    </OnUpdate>      
    <OnShutdown>         
      <CallFunction name="ThinkOutLoud.Shutdown" />      
    </OnShutdown>          
  </UiMod>
</ModuleFile>