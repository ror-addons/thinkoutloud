-- ThinkOutLoud - Announces to specified chat channels when you use abilities
--
-- Author: Thanners 20080917
-- vim:ts=4:sw=4:sts=4:et:
--

local tinsert = table.insert
local VERSION = {major=1,minor=1,patch=5}

ThinkOutLoud = {
    CMD_SAY = 1,
    CMD_TELL = 2,
    CMD_PARTY = 3,
    CMD_EMOTE = 4,
    CMD_SHOUT = 5,
    CMD_WARBAND= 6,
    CMD_SCENARIO = 7,
    CMD_SCENPARTY = 8,
    CMD_SMART = 9,

    CHANNELS = {
        [1] = "SAY",
        [2] = "TELL",
        [3] = "PARTY",
        [4] = "EMOTE",
        [5] = "SHOUT",
        [6] = "WARBAND",
        [7] = "SCENARIO",
        [8] = "SCENPARTY",
        [9] = "SMARTCHANNEL",
    },

    -- events we could (potentially) handle, as shown to a user.
    USER_EVENTS = {
        L"SPELLCAST",
        --[[
        -- dealing/receiving hits/crits,
        -- parry/block/absorb..
        -- ...
        --]]
    },

    -- current time
    timenow=nil,
    -- last time we spoke for a particular skill use.
    skillLastSpeakTime={},
    -- minimum time before we speak again for the same skill.
    skillThrottleTime=1.4,
    -- last time we spoke anything.
    lastSpeakTime=nil,

    -- If we're not valid, don't save settings or we might overwrite settings
    -- that just didn't load properly.
    valid=false,
}

local EVENTS = {
    [SystemData.Events.PLAYER_BEGIN_CAST] = "ThinkOutLoud.OnCast",
}

ThinkOutLoud.CHANNEL_SWITCHER = {
    [ThinkOutLoud.CMD_SAY] = L"/s",
    [ThinkOutLoud.CMD_TELL] = L"/t %F",
    [ThinkOutLoud.CMD_PARTY] = L"/p",
    [ThinkOutLoud.CMD_EMOTE] = L"/em",
    [ThinkOutLoud.CMD_SHOUT] = L"/sh",
    [ThinkOutLoud.CMD_WARBAND] = L"/wb",
    [ThinkOutLoud.CMD_SCENARIO] = L"/sc",
    [ThinkOutLoud.CMD_SCENPARTY] = L"/sp",
}



local function Print(txt)
    if "string" == type(txt) then
        EA_ChatWindow.Print(L"TOL:"..StringToWString(txt))
    else
        EA_ChatWindow.Print(L"TOL:"..txt)
    end
end


local function GetSmartChannel()
    local switcher = ThinkOutLoud.CMD_SAY
    local groupData = GetGroupData()
    for k,v in pairs(groupData) do
        if v and v.name and v.name ~= L"" then
            switcher = ThinkOutLoud.CMD_PARTY
        end
    end

    if IsWarBandActive() then
        switcher = ThinkOutLoud.CMD_WARBAND
    end
    if GameData.Player.isInScenario and GameData.Player.isInSiege then
        switcher = ThinkOutLoud.CMD_WARBAND
    end
    if GameData.Player.isInScenario and not GameData.Player.isInSiege then
        switcher = ThinkOutLoud.CMD_SCENARIO
    end
    
    return switcher
end


local function SendChat(phrase, target, friend)
    local s = ThinkOutLoud.ReplaceVariables(phrase.text, target, friend)
    local channel = phrase.channel
    if ThinkOutLoud.CMD_SMART == channel then
        channel = GetSmartChannel()
    end

    local switcher = ThinkOutLoud.CHANNEL_SWITCHER[channel]
    if (GameData.Player.isInScenario) and (switcher == "/p" or switcher == "/wb") then switcher = L"/sc" end
    switcher = ThinkOutLoud.ReplaceVariables(switcher, target, friend)

    if ThinkOutLoud.Settings.isMute then
        Print(L"[muted] "..switcher..L" "..s)
        return
    end

   message = switcher..L" "..s
   SendChatText(towstring(message), L"")
end 


-- This one is only to try to prevent speech triggered by the same skill
-- within some short time.
function ThinkOutLoud.IsDoubleCast(skill)
    local now = ThinkOutLoud.timenow
    local earlier = ThinkOutLoud.skillLastSpeakTime[skill.name]
    if not now then return end
    if earlier and (now - earlier) < ThinkOutLoud.skillThrottleTime then
        return true
    else
        return false
    end
end


-- This one is for the user to prevent themselves from spamming too
-- many times in a short time. isUrgent skills will bypass this one.
function ThinkOutLoud.IsTooSoon()
    local now = ThinkOutLoud.timenow
    local earlier = ThinkOutLoud.lastSpeakTime
    if not now then return end
    if earlier and (now - earlier) < ThinkOutLoud.Settings.throttleTime then
        return true
    else
        return false
    end
end


function ThinkOutLoud.SkillSpeakChance(skill)
    if ThinkOutLoud.IsDoubleCast(skill) then
        return 0.00
    elseif skill.isUrgent then
        return 1.00
    elseif ThinkOutLoud.IsTooSoon() then
        return 0.00
    else
        return skill.speakChance or ThinkOutLoud.Settings.speakChance
    end
end


function ThinkOutLoud.ReplaceVariables(s, target, friend)
    local replace
    if target == L"" then
        replace = L"<notarget>"
    else
        replace = target
    end
    s = s:gsub(L"%%[Tt]", replace)
    if friend == L"" then
        replace = L"<notarget>"
    else
        replace = friend
    end
    s = s:gsub(L"%%[Ff]", replace)
    return s
end


function ThinkOutLoud.OnCast(actionId, isChannel, desiredCastTime, averageLatency)
    local target = TargetInfo:UnitName("selfhostiletarget")
    local targetprof = TargetInfo:UnitCareerName("selfhostiletarget")
    target = (target..L" ("..targetprof..L")") or L""

    local friend = TargetInfo:UnitName("selffriendlytarget")
    local friendprof = TargetInfo:UnitCareerName("selffriendlytarget")    
    friend = friend:match(L"(.*)\^(.*)") or L""

    local playername = GameData.Player.name:match(L"(.*)\^(.*)")

    local isTargettingSelf = (friend == playername or friend == L"")

    local data = Player.GetAbilityData(actionId)
    if not data then return end
    local name = data.name:match(L"(.*)\^(.*)")
    local skill = ThinkOutLoud.Settings.skills[name]
    if skill then
        local r = math.random()
        if r > ThinkOutLoud.SkillSpeakChance(skill) then
            return
        end
        local phrase = skill:randomPhrase(
            ThinkOutLoud.Settings.tags,
            isTargettingSelf
        )
        if phrase then
            SendChat(phrase, target, friend)
            ThinkOutLoud.skillLastSpeakTime[skill.name] = ThinkOutLoud.timenow
            ThinkOutLoud.lastSpeakTime = ThinkOutLoud.timenow
        end
    end
end


-------
-- Annoying stuff
------------------

function ThinkOutLoud.SetMute(isMute)
    ThinkOutLoud.Settings.isMute = isMute
    if isMute then
        Print("mute: ON")
    else
        Print("mute: OFF")
    end
end


function ThinkOutLoud.SetSpeakChance(speakChance)
    speakChance = tonumber(speakChance)
    if speakChance < 0 or speakChance > 1 then
        Print("speakchance must be between 0.0 and 1.0 (ideally less than 0.10)")
        return
    end
    ThinkOutLoud.Settings.speakChance = speakChance
    Print("speakchance: "..speakChance)
    if speakChance > 0.10 then
        Print("WARNING. speakchance greater than 10% can become Very Spammy.")
    end
end


function ThinkOutLoud.HandleSlashCommands(args)
    local opt, val = args:match("([a-zA-Z0-9]+)[ ]?(.*)")
    if "resetallsettings" == opt then
        ThinkOutLoud.InitSettings()
        ThinkOutLoud.LoadSettings(TOL_CONFIGS["DEFAULT"])
        Print("All settings and skills reset to default")
    elseif "loadtest" == opt then
        ThinkOutLoud.InitSettings()
        ThinkOutLoud.LoadSettings(TOL_CONFIGS["thanners-shaman"])
    elseif "config" == opt then
        TOLSettingsUI.SetShowing(true)
    elseif "hide" == opt then
        TOLSettingsUI.SetShowing(false)
    elseif "mute" == opt then
        ThinkOutLoud.SetMute(true)
    elseif "unmute" == opt then
        ThinkOutLoud.SetMute(false)
    elseif "speakchance" == opt then
        if val ~= "" then
            ThinkOutLoud.SetSpeakChance(val)
        else
            Print("speakchance: "..ThinkOutLoud.Settings.speakChance)
        end
    elseif "throttle" == opt then
        if val ~= "" then
            ThinkOutLoud.Settings.throttleTime = tonumber(val) or 0
            Print("throttle set to "..ThinkOutLoud.Settings.throttleTime)
        else
            Print("throttle: "..ThinkOutLoud.Settings.throttleTime)
        end
    else
        Print("Usage:")
        Print(" You may use /tol or /thinkoutloud")
        Print(" /tol resetallsettings   (reload settings from config.lua)")
        Print(" /tol mute|unmute")
        Print(" /tol speakchance CHANCE  (default: 0.05)")
        Print(" /tol throttle TIME       (default: 0 seconds)")
        Print(" /tol config  (open up the configuration GUI)")
    end
end
    

function ThinkOutLoud.RegisterEventHandlers()
    for e,h in pairs(EVENTS) do
        RegisterEventHandler(e, h)
    end
end


function ThinkOutLoud.UnregisterEventHandlers()
    for e,h in pairs(EVENTS) do
        UnregisterEventHandler(e, h)
    end
end


function ThinkOutLoud.SetSetting(key, value)
    local player = WStringToString(GameData.Player.name)
    local settings = ThinkOutLoud.Settings[player]
    settings[key] = value
end


function ThinkOutLoud.GetSetting(key)
    local player = WStringToString(GameData.Player.name)
    local settings = ThinkOutLoud.Settings[player]
    if settings[key] ~= nil then
        return settings[key]
    else
        if ThinkOutLoud.DefaultSettings[key] ~= nil then
            settings = ThinkOutLoud.DefaultSettings[key]
            return settings[key]
        else
            return nil
        end
    end
end


function ThinkOutLoud.InitSettings()
    ThinkOutLoud.Settings = {
        version = VERSION,
        speakChance = 0.05,
        isMute = false,
        skills = {},
        tags = {},
        throttleTime=0,
    }
    -- all tags on by default.
    ThinkOutLoud.Settings.tags = TOLSet.new(ThinkOutLoud.CHANNELS)
end


-- TODO: Write functions for handling the version comparison
function ThinkOutLoud.LoadSettings(loadSettings)
    ThinkOutLoud.UpdateOldSettings(loadSettings)
     
    -- fill in other Settings
    if loadSettings.speakChance then
        ThinkOutLoud.Settings.speakChance = loadSettings.speakChance
    end
    if loadSettings.throttleTime then
        ThinkOutLoud.Settings.throttleTime = loadSettings.throttleTime
    end

    -- populate skills table
    for _,v in pairs(loadSettings.skills) do
        local skill = TOLSkill.new(v.name, v.isUrgent, v.speakChance)
        for _, phrase in pairs(v.phrases) do
            skill:insertPhrase(phrase)
        end
        ThinkOutLoud.Settings.skills[v.name] = skill
    end
    ThinkOutLoud.valid = true
end


function ThinkOutLoud.UpdateOldSettings(loadSettings)
    local oldver = loadSettings.version
    -- XXX
end

function ThinkOutLoud.SaveSettings()
    if not ThinkOutLoud.valid then
        return
    end
    local settings = ThinkOutLoud.Settings
    local save = {}
    save.version = settings.version
    save.speakChance = settings.speakChance
    save.throttleTime = settings.throttleTime
    save.skills = {}
    for k,v in pairs(settings.skills) do
        tinsert(save.skills, v:save())
    end
    ThinkOutLoud.PersistentSettings = save
end


function ThinkOutLoud.Initialize()
    ThinkOutLoud.RegisterEventHandlers()
    local slashes = {"tol", "thinkoutloud"}
    for _, s in pairs(slashes) do
        local r = LibSlash.RegisterSlashCmd(s, ThinkOutLoud.HandleSlashCommands)
        if not r then
            Print("Failed to register '"..s.."'")
        end
    end
    ThinkOutLoud.InitSettings()
    if not ThinkOutLoud.PersistentSettings then
        ThinkOutLoud.LoadSettings(TOL_CONFIGS["DEFAULT"])
    else
        ThinkOutLoud.LoadSettings(ThinkOutLoud.PersistentSettings)
    end
    TOLSettingsUI.Initialize()
    d("Initialised.")
end


function ThinkOutLoud.Update(elapsed)
    if not ThinkOutLoud.timenow then
        ThinkOutLoud.timenow = 0
    end
    ThinkOutLoud.timenow = ThinkOutLoud.timenow + elapsed
end


function ThinkOutLoud.Shutdown()
    ThinkOutLoud.SaveSettings()
    TOLSettingsUI.Shutdown()
    ThinkOutLoud.UnregisterEventHandlers()
end

